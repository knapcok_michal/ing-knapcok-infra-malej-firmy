AWSTemplateFormatVersion: 2010-09-09

Description: JUMP - Test

Parameters:
  ParentVpcStack:
    Description: ParentVpcStack
    Type: String

  ImageId:
    Description: Parameter name for imageid of given OS type
    Type: String
#    Default: ami-0986f08f184c24871 # davincicloud-encrypted-baseami-amazonlinux-06-03-2020-19-42-14 made from  ami-0e61341fa75fcaa18
    Default: ami-003f19e0e687de1cd
  InstanceType:
    Description: InstanceType
    Type: String
    Default: t3.micro
    AllowedValues:
      - t3.micro
      - t3.small
      - t3.medium

  KeyName:
    Description: KeyName for administrator
    Type: AWS::EC2::KeyPair::KeyName
    Default: mknapcokKEY
    MinLength: 1

#  IamSshAssumeRole:
#    Description: Assume role for getting IAM users
#    Type: String
#    Default: arn:aws:iam::955305051178:role/IAMSSHKeysGetRole
#  IamAuthorizedGroups:
#    Description: Comma separated list of IAM Groups of users created on EC2 Instance in ECS Cluster
#    Default: Operators
#    Type: String
#  SudoersGroups:
#    Description: Comma separated list of IAM Groups of users created on EC2 Instance in ECS Cluster with SUDO Access
#    Default: Operators
#    Type: String

Resources:
  JumpPrivateSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: JumpPrivateSecurityGroup
      VpcId:
        Fn::ImportValue: !Sub ${ParentVpcStack}-VpcId
      SecurityGroupIngress:
        - { IpProtocol: tcp, FromPort: 22, ToPort: 22, CidrIp: 10.0.0.0/8, Description: INTERNAL_SSH }
        - { IpProtocol: tcp, FromPort: 22, ToPort: 22, CidrIp: 80.242.33.206/32, Description: SKLENE_SSH}
        - { IpProtocol: icmp, FromPort: -1, ToPort: -1, CidrIp: 0.0.0.0/0, Description: ICMP }
      SecurityGroupEgress:
        - { IpProtocol: tcp, FromPort: 80, ToPort: 80, CidrIp: 0.0.0.0/0, Description: HTTP }
        - { IpProtocol: tcp, FromPort: 443, ToPort: 443, CidrIp: 0.0.0.0/0, Description: HTTPS }
        - { IpProtocol: tcp, FromPort: 636, ToPort: 636, CidrIp: 0.0.0.0/0, Description: LDAPS }
        - { IpProtocol: icmp, FromPort: -1, ToPort: -1, CidrIp: 0.0.0.0/0, Description: ICMP }

  JumpLaunchConfiguration:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      ImageId: !Ref ImageId
      KeyName:
        !Ref KeyName
      SecurityGroups:
        - !Ref JumpPrivateSecurityGroup
      InstanceType: !Ref InstanceType
#      IamInstanceProfile:
#        Fn::ImportValue: !Sub ${ParentIamStack}-JumpInstanceProfile
      BlockDeviceMappings:
        - DeviceName: /dev/sde
          Ebs:
            DeleteOnTermination: true
            VolumeType: standard
            VolumeSize: 5
            Encrypted: true
      UserData: !Base64 |
        #!/bin/bash -x
          sudo apt-get update
          sudo apt-get install dnsutils -y

  JumpScalingGroup:
    Type : AWS::AutoScaling::AutoScalingGroup
    UpdatePolicy:
      AutoScalingRollingUpdate:
        MinInstancesInService: 0
        MaxBatchSize: 1
        PauseTime: PT0M5S
        SuspendProcesses:
          - ScheduledActions
    Properties:
      AvailabilityZones:
        - us-east-1a
        - us-east-1b
      VPCZoneIdentifier:
        - !Select [ 0, !Split [ ',', 'Fn::ImportValue': !Sub '${ParentVpcStack}-DmzSubnetIDs' ]]
        - !Select [ 1, !Split [ ',', 'Fn::ImportValue': !Sub '${ParentVpcStack}-DmzSubnetIDs' ]]
      LaunchConfigurationName: !Ref JumpLaunchConfiguration
      Cooldown: 600
      HealthCheckGracePeriod: 600
      HealthCheckType: ELB
      DesiredCapacity: 1
      MinSize: 1
      MaxSize: 2
      TargetGroupARNs:
        - !Ref JumpPublicLoadBalancerTargetGroupSSH
#        - !Ref JumpPublicLoadBalancerTargetGroupOPENVPN
      MetricsCollection:
        - Granularity: 1Minute  #The frequency at which Amazon EC2 Auto Scaling sends aggregated data to CloudWatch.
          Metrics:
            - GroupMinSize
            - GroupMaxSize

  JumpPublicLoadBalancer:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Subnets:
        - !Select [ 0, !Split [ ',', 'Fn::ImportValue': !Sub '${ParentVpcStack}-PublicSubnetIDs' ]]
        - !Select [ 1, !Split [ ',', 'Fn::ImportValue': !Sub '${ParentVpcStack}-PublicSubnetIDs' ]]
      Scheme: internet-facing
      Type: network
      IpAddressType: ipv4
      LoadBalancerAttributes:
        - Key: load_balancing.cross_zone.enabled
          Value: true

  JumpPublicLoadBalancerTargetGroupSSH:   
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckIntervalSeconds: 30
      HealthCheckPort: 22
      HealthCheckProtocol: TCP
      HealthyThresholdCount: 3
      UnhealthyThresholdCount: 3
      TargetType: instance
      VpcId:
        Fn::ImportValue: !Sub ${ParentVpcStack}-VpcId
      Port: 22
      Protocol: TCP
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: 60

  JumpPublicLoadBalancerListenerSSH:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      LoadBalancerArn:
        !Ref JumpPublicLoadBalancer
      Port: 22
      Protocol: TCP
      DefaultActions:
        - TargetGroupArn:
            !Ref JumpPublicLoadBalancerTargetGroupSSH
          Type: forward

  JumpPublicLoadBalancerDnsRecord:
    Type: AWS::Route53::RecordSetGroup
    Properties:
      HostedZoneId:
        Fn::ImportValue: !Sub ${ParentVpcStack}-PublicHostedZoneId
      Comment: JumpPublicLoadBalancerDnsRecord
      RecordSets:
        - Name: !Join [ ".", ['jump' , 'Fn::ImportValue': !Sub '${ParentVpcStack}-PublicHostedZoneName']]
          Type: A
          AliasTarget:
            HostedZoneId: !GetAtt JumpPublicLoadBalancer.CanonicalHostedZoneID
            DNSName: !GetAtt JumpPublicLoadBalancer.DNSName

  JumpPrivateLoadBalancerDnsRecord:
    Type: AWS::Route53::RecordSetGroup
    Properties:
      HostedZoneId:
        Fn::ImportValue: !Sub ${ParentVpcStack}-PrivateHostedZoneId
      Comment: JumpPrivateLoadBalancerDnsRecord
      RecordSets:
        - Name: !Join [ ".", ['jump' , 'Fn::ImportValue': !Sub '${ParentVpcStack}-PrivateHostedZoneName']]
          Type: A
          AliasTarget:
            HostedZoneId: !GetAtt JumpPublicLoadBalancer.CanonicalHostedZoneID
            DNSName: !GetAtt JumpPublicLoadBalancer.DNSName