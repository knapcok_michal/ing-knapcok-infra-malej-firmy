#!/bin/bash
sudo apt-get install git -y
git clone https://github.com/OpenVPN/easy-rsa.git
cd easy-rsa/easyrsa3
./easyrsa init-pki
echo ""|./easyrsa build-ca nopass
./easyrsa build-server-full server nopass
./easyrsa build-client-full client.domain.tld nopass
mkdir ~/customCertificates/
cp pki/ca.crt ~/customCertificates/
cp pki/issued/server.crt ~/customCertificates/
cp pki/private/server.key ~/customCertificates/
cp pki/issued/client.domain.tld.crt ~/customCertificates/
cp pki/private/client.domain.tld.key ~/customCertificates/
cd ~/customCertificates/
sudo aws acm import-certificate --certificate fileb://server.crt --private-key fileb://server.key --certificate-chain fileb://ca.crt --region us-east-1
sudo aws acm import-certificate --certificate fileb://client.domain.tld.crt --private-key fileb://client.domain.tld.key --certificate-chain fileb://ca.crt --region us-east-1